@include('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Ubah Profile</h1>

                <form method="post" action="/{{ $profile->id }}/edit">
                    @csrf
                    @method('patch')
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $profile->nama }}">
                    </div>
                    <div class="mb-3">
                        <label for="pekerjaan" class="form-label">pekerjaan</label>
                        <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value="{{ $profile->pekerjaan }}">
                    </div>
                    <div class="mb-3">
                        <label for="tanggal_lahir" class="form-label">Tanggal lahir</label>
                        <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{ $profile->tgl_lahir }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                    <a href="/" class="btn btn-secondary">Kembali</a>
                </form>
            </div>
        </div>
    </div>
@show
