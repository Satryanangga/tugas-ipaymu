@include('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10">
                @include('alert')
                <h1 class="mt-3">Daftar Profile</h1>

                @include('profil.tambah')

                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Pekerjaan</th>
                            <th scope="col">Tanggal Lahir</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($profiles as $profile)
                        <tr>
                            <td scope="row">{{ $loop->iteration }}</td>
                            <td scope="row">{{ $profile->nama }}</td>
                            <td scope="row">{{ $profile->pekerjaan }}</td>
                            <td scope="row">{{ $profile->tgl_lahir }}</td>
                            <td>
                                <a href="/{{$profile->id}}/edit" class="btn btn-warning">Edit</a>
                                <form action="/{{ $profile->id }}/delete" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $profiles->links() }}
            </div>
        </div>
    </div>
@show
