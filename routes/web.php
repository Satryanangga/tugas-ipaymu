<?php

use App\Http\Controllers\ProfilesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [ProfilesController::class, 'index']);
Route::post('/', [ProfilesController::class, 'store']);
Route::get('/{profile}/edit', [ProfilesController::class, 'edit']);
Route::patch('/{profile}/edit', [ProfilesController::class, 'update']);
Route::delete('/{profile}/delete', [ProfilesController::class, 'destroy']);


// Route::get('/payment', [PaymentsController::class, 'index']);

Route::get('/payment', function(){
    return view('payment.page');
});
