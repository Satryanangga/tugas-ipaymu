<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Uuids;

class Profile extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = ['nama', 'pekerjaan', 'tgl_lahir'];
    // protected $primaryKey = 'id';
    public $incrementing = false;

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function($query, $search) {
            return $query->where('nama', 'like', '%' . $search . '%')
            ->orWhere('pekerjaan', 'like', '%' . $search . '%');
        });
    }
    // public $timestamps = false;
}
